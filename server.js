// server.js

// set up ========================
var express  = require('express');
var app      = express();                               // create our app w/ express
var port  = process.env.PORT || 8080;
var mongoose = require('mongoose');                     // mongoose for mongodb
var morgan = require('morgan');             // log requests to the console (express4)
var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
var cookieParser = require('cookie-parser');
var session      = require('express-session');
var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)
var passport = require('passport');
var flash    = require('connect-flash');

// configuration =================
// load the config
var database = require('./config/database');
require('./config/passport')(passport); // pass passport for configuration
// mongoose.connect(process.env.MONGO_URL);     // connect to mongoDB database on modulus.io
mongoose.connect(database.url);     // connect to mongoDB database on modulus.io

app.use(express.static(__dirname + '/public'));                 // set the static files location /public/img will be /img for users
app.use(morgan('dev'));                                         // log every request to the console
app.use(cookieParser());										// read cookies (needed for auth)
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(methodOverride());

app.set('view engine', 'ejs'); // set up ejs for templating
app.set('views', './public/views'); // par defaut, ejs utiliser ./view, on veut le changer a ./public/views

// required for passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// load the routes
require('./app/routes')(app, passport); // load our routes and pass in our app and fully configured passport

// listen (start app with node server.js) ======================================
app.listen(port);
console.log("App listening on port %s", port);
